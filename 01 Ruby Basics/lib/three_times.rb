
# Now that you have Ruby installed and have written your first program,
# try writing another program that prints your name three times. This will
# involve the use of iteration! Check out the documentation for the "times"
# method for help.

def three_times
  3.times { puts 'David' }
end
