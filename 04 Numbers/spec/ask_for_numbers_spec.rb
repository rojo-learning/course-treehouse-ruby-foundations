
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/ask_for_numbers'

describe '#format_as_money' do
  it 'formats integers' do
    num = rand(101)
    aux = num.to_s + '.00'
    format_as_money(num).must_equal aux
  end

  it 'formats floats' do
    num = 23.45678
    aux = "#{sprintf '%.2f', num}"
    format_as_money(num).must_equal aux
  end
end
