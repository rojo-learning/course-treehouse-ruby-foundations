
# Write a program that prints out the items inside of the array in a sorted
# order.

def run!
  array = 10.times.map { rand 10 + 1 }
  sorted_print array
  puts
end

def sorted_print(array)
  print array.sort
end

run! if __FILE__ == $0
