
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/remove_punctuation'

describe '#akward_text' do
  it 'reverses a given string' do
    str = 'Mundo enfermo y triste'
    akward_text(str).must_equal str.reverse
  end

  it 'removes punctuation from the string' do
    str = '¡Es un mundo enfermo y triste!'
    aux = 'Es un mundo enfermo y triste'.reverse
    akward_text(str).must_equal aux
  end
end
