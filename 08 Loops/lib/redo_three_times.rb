
# Try writing a program that uses a redo statement to only redo something
# three times. You'll have to keep track of the retry count using another
# internal variable.

def stutter(text)
  max_reps = 3 # Repetitions on the stuttered word go from 1 to max_reps.
  chance   = 3 # Probability of a word being stuttered is 1 / chance.

  stuttered = text.split.map do |word|
    if rand(chance).zero?
      reps = (rand max_reps) + 1
      stutter_word(word, reps)
    else
      word
    end
  end

  stuttered.join(' ')
end

def stutter_word(word, reps)
  new_word = ''
  for n in (0...word.size) do
    new_word += word[n]

    if word[n] =~ /[^aeiou]/i and reps > 0
      reps     -= 1
      if reps > 0
        new_word += '-' and redo
      else
        next
      end
    end
  end

  new_word
end

# stutter 'Hello! I just wanted to tell you that I like you.'
# ~> "Hello! I just wanted to tell you t-t-that I l-like y-you."
