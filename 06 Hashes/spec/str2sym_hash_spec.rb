
require 'minitest/autorun'
require 'minitest/spec'

require_relative '../lib/str2sym_hash'

describe '#srt_keys2sym_keys' do
  it 'works with hashes that have all keys as strings' do
    hsh = { 'name' => 'David', 'family' => 'Rojo', 'age' => 33 }
    out = { name: 'David', family: 'Rojo', age: 33 }
    srt_keys2sym_keys(hsh).must_equal out
  end

  it 'works with hashes that have some keys as strings' do
    hsh = { 'name' => 'David', :family => 'Rojo', 'age' => 33 }
    out = { name: 'David', family: 'Rojo', age: 33 }
    srt_keys2sym_keys(hsh).must_equal out
  end
end
