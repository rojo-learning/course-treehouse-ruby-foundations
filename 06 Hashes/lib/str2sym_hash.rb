
# Now that we know how hashes work, write a function that converts hashes with
# string keys in to hashes with symbols as keys. The function should work
# recursively.

def run!
  h = { 'name' => 'David', 'family' => 'Rojo', 'age' => 33 }
  puts srt_keys2sym_keys(h)
end

def srt_keys2sym_keys(hash)
  if hash.empty?
    hash
  else
    key, value = hash.shift
    srt_keys2sym_keys(hash).merge({ key.to_sym => value })
  end
end

run! if __FILE__ == $0
