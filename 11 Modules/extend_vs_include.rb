
# Example on the use of extend and include with modules.

module FormatAttributes
  def formats(*attributes)
    @format_attributes = attributes
  end

  def format_attributes
    @format_attributes
  end
end

module Formatter
  def display
    self.class.format_attributes.each do |attribute|
      puts "[#{attribute.to_s.upcase}] #{send(attribute)}"
    end
  end
end

class Resume
  attr_accessor :name, :phone, :email, :experience

  extend  FormatAttributes
  include Formatter

  formats :name, :phone, :email, :experience
end

class CV
  attr_accessor :name, :experience

  extend  FormatAttributes
  include Formatter

  formats :name, :experience
end

resume = Resume.new
resume.name  = 'David Rojo'
resume.email = 'david.orojo@gmail.com'
resume.phone = '461 1343433'
resume.experience = 'Ruby'

resume.display

puts '---------------'

cv = CV.new
cv.name = 'David Rojo'
cv.experience = 'Ruby'

cv.display
