
# In this lesson we created a very simple formatting module. Try writing a more
# complicated formatting module that will output either HTML or Markdown.

module FormatAttributes
  def formats(attributes)
    @format_attributes = attributes
  end

  def format_attributes
    @format_attributes
  end
end

module Formatter
  def display
    self.class.format_attributes.each do |key, value|
      puts SimpleXML.build_tag(value.to_s, id: key.to_s)
    end
  end

  class SimpleXML
    class << self
      def build_tag(name, attr_hash, &inner)
        tag  = open_tag(name) + tag_attrs(attr_hash)
        tag += (inner) ? close_tag + inner.call + tail(name) : close_tag(:self)
      end

      private
      def open_tag(tag)
        "<#{tag}"
      end

      def tag_attrs(attrs)
        ''.tap { |str| attrs.each { |k, v| str << " #{k}='#{v}'" } }
      end

      def close_tag(type = :tail)
        (type == :self) ? '/>' : '>'
      end

      def tail(tag)
        "</#{tag}>"
      end
    end
  end
end

class Resume
  attr_accessor :name, :phone, :email, :experience

  extend  FormatAttributes
  include Formatter

  formats name: :li, phone: :li, email: :li, experience: :p
end

def run!
  resume = Resume.new
  resume.name  = 'David Rojo'
  resume.email = 'david.orojo@gmail.com'
  resume.phone = '461 1343433'
  resume.experience = 'Ruby'

  resume.display
end

run! if __FILE__ == $0
